Rails.application.routes.draw do
  root to: 'visitors#index'
  devise_for :users, controllers: {
    registrations: "devise/custom_registrations"
  }
  resources :users
end
