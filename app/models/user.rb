class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name,    presence: true
  validates :address, presence: true
  validates :phone,   presence: true
  validates :role,    presence: true,
                      inclusion: { in: %w(company veterinary) }

  validates :cnpj,    presence: true, cnpj: true, if: 'role == "company"'
  validates :crmv,    presence: true,
                      numericality: { only_integer: true },
                      length: { maximum: 5 }, if: 'role == "veterinary"'

  before_validation :ensure_role

  private
  def ensure_role
    self.role = 'company' if role.blank?
  end
end
