class Devise::CustomRegistrationsController < Devise::RegistrationsController
  before_action :configure_permitted_parameters

  protected

  # Add custom fields User Model to Devise
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: signup_params)
    devise_parameter_sanitizer.permit(:account_update, keys: account_update_params)
  end

  private
    def account_update_params
      [:name, :email, :address, :phone, :cnpj, :crmv]
    end

    def signup_params
      [:name, :email, :address, :phone, :cnpj, :crmv, :password, :password_confirmation, :role]
    end
end
