FactoryGirl.define do
  factory :user do
    # name "Test User"
    # email "test@example.com"
    # password "please123"
    sequence(:name) { |n| "name#{('A'..'Z').to_a.at(n%23)}".downcase }
    sequence(:email) { |n| "person#{n}@example.com".downcase }
    password "secret123"
    password_confirmation "secret123"
    address 'Endereco'
    phone '5511999996633'

    factory :company_user do
      role 'company'
      cnpj '48.147.870/0001-14'
    end

    factory :veterinary_user do
      role 'veterinary'
      crmv '98756'
    end
  end
end
