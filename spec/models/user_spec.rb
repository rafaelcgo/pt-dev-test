describe User do

  # before(:each) { @user = User.new(email: 'user@example.com') }

  # subject { @user }

  # it { should respond_to(:email) }

  # it "#email returns a string" do
  #   expect(@user.email).to match 'user@example.com'
  # end

  context 'company user' do
    it 'should have valid factory' do
      expect(build(:company_user)).to be_valid
    end

    it 'should require name' do
      expect(build(:company_user, name: nil)).to_not be_valid
    end

    it 'should require email' do
      expect(build(:company_user, email: nil)).to_not be_valid
    end

    it 'should require address' do
      expect(build(:company_user, address: nil)).to_not be_valid
    end

    it 'should require phone' do
      expect(build(:company_user, phone: nil)).to_not be_valid
    end

    it 'should have unique email' do
      user = create(:company_user)
      expect(build(:company_user, email: user.email)).to_not be_valid
    end

    it 'should have valid role' do
      expect(build(:company_user, role: 'invalid')).to_not be_valid
    end

    it 'should have a valid CNPJ' do
      user = create(:company_user)
      expect(build(:company_user, cnpj: '48.147.870/0001-00')).to_not be_valid
    end
  end

  context 'veterinary user' do
    it 'should not have letters in crmv' do
      expect(build(:veterinary_user, crmv: 'crmv123')).to_not be_valid
    end
    it 'should not have more than 5 digits in crmv' do
      expect(build(:veterinary_user, crmv: '123456')).to_not be_valid
    end
  end

  context 'before_validation' do
    context ':ensure_role' do
      it 'should always have role defined before validation' do
        user = build(:user, role: nil)
        user.valid?
        expect(user.role).to_not be_nil
      end
    end
  end

  context 'private methods' do
    context '.ensure_role' do
      it 'should set role to "company" if empty' do
        user = build(:user, role: nil)
        user.send(:ensure_role)
        expect(user.role).to eq('company')
      end

      it 'should not change role if role == "company"' do
        user = build(:user, role: 'company')
        user.send(:ensure_role)
        expect(user.role).to eq('company')
      end

      it 'should not change role if role == "veterinary"' do
        user = build(:user, role: 'veterinary')
        user.send(:ensure_role)
        expect(user.role).to eq('veterinary')
      end
    end
  end


end
