class AddNameAddressPhoneCnpjCrmvRoleToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :name, :string
    add_column :users, :address, :text
    add_column :users, :phone, :string
    add_column :users, :cnpj, :string
    add_column :users, :crmv, :string
    add_column :users, :role, :string
  end
end
